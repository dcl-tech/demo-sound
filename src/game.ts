/////////////////////////////////////////////////////////
// demo-sound
// You are welcome to use any content from this open source scene in compliance with the license.
/*
Copyright 2019 Carl Fravel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/////////////////////////////////////////////////////////

import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'

// Create entity which when touched starts and stops the rhythm
const rhythmCube = spawnBoxX(8,1,8,  0,0,0,  1,1,1)

const blackMaterial = new Material()
blackMaterial.albedoColor = Color3.Black()

const greenMaterial = new Material()
greenMaterial.albedoColor = Color3.Green()

rhythmCube.addComponentOrReplace(blackMaterial)

// sounds I recorded in my office by tapping varous objects, or vocalizing
let clips:AudioClip[] = [
  new AudioClip('sounds/Bass1.mp3'),
  new AudioClip('sounds/Knock1.mp3'),
  new AudioClip('sounds/Knock2-little.mp3'),
  new AudioClip('sounds/Knock3.mp3'),
  new AudioClip('sounds/Knock4.mp3'),
  new AudioClip('sounds/Knock5-little.mp3'),
  new AudioClip('sounds/Knock6.mp3'),
  new AudioClip('sounds/LittleBump1.mp3'),
  new AudioClip('sounds/MetalPlate1.mp3'),
  new AudioClip('sounds/MetalPlate2.mp3'),
  new AudioClip('sounds/MetalPlate3.mp3'),
  new AudioClip('sounds/Slap1.mp3'),
  new AudioClip('sounds/Tamp1.mp3'),
  new AudioClip('sounds/Tick1.mp3'),
  new AudioClip('sounds/Tump1.mp3'),
  new AudioClip('sounds/Tump2.mp3'),
  new AudioClip('sounds/Tump3-little.mp3'),
  new AudioClip('sounds/Uh1.mp3'),
  new AudioClip('sounds/Uh2.mp3'),
  new AudioClip('sounds/Uh3.mp3'),
  new AudioClip('sounds/Whack1.mp3')
]

// This system plays sounds rhythmically
class RhythmSystem implements ISystem {
  playing:boolean = false
  tempo = 120
  nextTime:number = 0 // first sound immediately
  audioSource:AudioSource = null

  constructor (public source:Entity) {
  }

  start(){
    this.nextTime = 0
    this.playing = true
  }

  stop(){
    this.playing = false
  }

  playSound(){
    // pick one of the sounds, randomly
    //let clip = clips[0]
    let clip = clips[String(Math.floor(Math.random()*clips.length))]

    // Create AudioSource component, referencing `clip`
    this.audioSource = new AudioSource(clip)

    // Add AudioSource component to entity
    this.source.addComponentOrReplace(this.audioSource)

    // sounds should not loop
    this.audioSource.loop = false

    // Play sound
    this.audioSource.playing = true
  }

  update(dt: number) {
    if (this.playing){
      if (this.nextTime - dt < 0) {
        this.playSound()
        // play a random sequence of the clips, each for 1 beat or 1/2 beat
        // (seconds per minute / beats per minute = seconds/beat) * (0.5|1.0)
        this.nextTime = (60  / this.tempo) * (Math.floor(Math.random()*2+1)/2) 
      }
      else {
        this.nextTime -= dt
      }
    }
  }
}

let rhythmSystem = new RhythmSystem(rhythmCube)

rhythmCube.addComponent(new OnClick(e => {
  if (rhythmSystem.playing){
    rhythmSystem.stop()
  }
  else{
    rhythmSystem.start()
  }
  rhythmCube.addComponentOrReplace(rhythmSystem.playing?greenMaterial:blackMaterial)
}))

// Add a new instance of the system to the engine
engine.addSystem(rhythmSystem)

